# 環境準備
1. 安裝 npm
> brew install nvm && nvm install v14.0.0

2. 安裝 vite
> npm install -g vite

3. 創立 vite 專案
> npm create vite

# 資料夾目錄介紹
0. .vscode: 使用 vscode 的工作區間設定
1. public: 不會被壓縮的檔案
2. src: 會被 vite 壓縮的檔案
    - assets: 資源，放置會被壓縮的圖檔
    - components: 放置代碼
    - App.vue: 頁面初始進入點
    - main.ts: 初始進入點
